//
//  ContentView.swift
//  pokedex
//
//  Created by Viorel Cojocari on 27/12/2021.
//

import SwiftUI
import WebKit


struct ContentView: View {
    
    //Variaveis
    @State var pokemon:Pokemon //Ir para pokemon
    @State var pokedex:String = "Pokedex"
    @State var cor:Color = Color.white
    @State private var showDetails = false
    
    var body: some View {
        
        NavigationView{
            
            ZStack{
                GifImage("capa")
                    .frame(width: 400.0, height: 800.0)
                
                VStack{
                    
                    Button("ABRIR"){
                        showDetails.toggle()
                    }
                    .foregroundColor(.black)
                    .padding(.vertical, 10)
                    .frame(width: 100.0, height: 40)
                    .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 2)
                    .offset(x: 0, y: 170)
                    if showDetails{
                        NavigationLink(destination: pokedexView()){
                            Text("Pokedex")
                        }
                        .padding()
                        .padding(.vertical, 10)
                        .frame(width: 200.0)
                        .border(Color.black, width: 2)
                        .offset(x: 0, y: 170)
                        
                        NavigationLink(destination: batalha(pokemon: Pokemon(nome:pokemon.nome, img: "\(pokemon.img)", atk: pokemon.atk, def:pokemon.def, hp:pokemon.hp, xp: 0, lvl: 0), cor: Color.white, cor2: Color.white)){
                            Text("Começar batalha")
                        }
                        .padding()
                        .padding(.vertical, 10)
                        .frame(width: 200.0)
                        .border(Color.black, width: 2)
                        .offset(x: 0, y: 170)
                    }
 
                }//VStack
                .padding(100.0)
                .scaledToFill().padding(-100)
                //.background(Image("wallpaper").resizable()
                //.padding(.bottom, 60.0))
            }
            
        }//NavegationView
    }//body
}//ContentView


struct ContentView_Previews: PreviewProvider {
    
    
    var arrayPokemon: [Pokemon] = []
    static var previews: some View {
        let pokemon1:Pokemon = Pokemon(nome: "Bulbasaur", img: "bulbasaur", atk:49, def:0.51, hp:100, xp: 0, lvl: 0)
        
        ContentView(pokemon: pokemon1)
    }
}//ContentView_Previews

