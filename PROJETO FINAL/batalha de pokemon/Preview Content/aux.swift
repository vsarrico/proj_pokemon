//
//  aux.swift
//  pokedex
//
//  Created by Viorel Cojocari on 27/12/2021.
//

import Foundation
import UIKit


extension UIImage{
    
    static var defImg:UIImage{
        UIImage(named: "noImg")!
    }
 
}
