//
//  Pokemon.swift
//  pokedex
//
//  Created by Viorel Cojocari on 27/12/2021.
//

import Foundation
import UIKit

/*
protocol Pokemon_Array {
    var nome: String { get }
}
*/

class Pokemon: Identifiable {
    
    var id = UUID()
    var nome:String
    var img:UIImage
    var atk:Int
    var def:Double
    var hp:Int
    var xp:Int
    var lvl:Int
    
    init(nome:String, img:String, atk:Int, def:Double, hp:Int, xp:Int, lvl:Int){
        self.nome = nome
        self.img = UIImage(named: img) ?? UIImage.defImg
        self.atk = atk
        self.def = def
        self.hp = hp
        self.xp = xp
        self.lvl = lvl
    }//init
}//Pokemon
