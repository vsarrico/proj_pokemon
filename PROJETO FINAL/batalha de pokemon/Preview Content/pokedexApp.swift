//
//  pokedexApp.swift
//  pokedex
//
//  Created by Viorel Cojocari on 27/12/2021.
//

import SwiftUI

@main
struct pokedexApp: App {
    
    var body: some Scene {
        let pokemon:Pokemon = Pokemon(nome: "Bulbasaur", img: "bulbasaur", atk:49, def:0.51, hp:100, xp: 0, lvl: 0)
        
        WindowGroup {
            ContentView(pokemon: pokemon)
        }
    }
}
