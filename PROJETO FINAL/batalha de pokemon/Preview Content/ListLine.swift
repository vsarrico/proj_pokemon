//
//  ListLine.swift
//  pokedex
//
//  Created by Viorel on 27/12/2021.
//

import SwiftUI

struct ListLine: View {
    @State var pokemon:Pokemon
    
    
    
    var body: some View {
      
        HStack{
            Image(uiImage: self.pokemon.img)
                .resizable()
                .scaledToFill()
                .frame(width: 60, height: 60)
            
            VStack{
                Text(self.pokemon.nome)
            }
        }
    }
     
}
