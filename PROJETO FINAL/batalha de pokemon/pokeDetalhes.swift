//
//  pokeDetalhes.swift
//  pokedex
//
//  Created by Viorel Cojocari on 07/12/2021.
//

import SwiftUI


struct pokeDetalhes: View {

    //Funcoes:
    /*
    func appendPokemon(pokemon:Pokemon) {
        arrayPokemon.append(Pokemon(nome:pokemon.nome, img: "\(pokemon.img)", atk: pokemon.atk, def:pokemon.def, hp:pokemon.hp))

        for element in arrayPokemon {
            print(element.nome)
        }
        print(arrayPokemon.count)
    }//appendPokemon
    */
    
    
    //Variaveis:
    //@State var arrayPokemon:[Pokemon]
    @State var pokemon:Pokemon
    @State var animate = false
    
    var body: some View {
        
            VStack(alignment: .center){
                
                //Image(uiImage: pokemon.img).resizable().frame(width: 200, height: 200)
                
                Text(pokemon.nome)
                .font(.custom("Helvetica Neue", size: 50))
                .padding(50)
                    
                VStack(alignment: .leading){
                    Group{
                        //Hp:
                        ProgressView(value: Float(pokemon.hp), total: 100.0){
                        Text("Vida:")
                            .font(.footnote)
                        }
                        .progressViewStyle(.linear)
                        .frame(width: 200.0)
                        .accentColor(.red)
                        
                        //Ataque:
                        ProgressView(value: Float(pokemon.atk), total: 100.0){
                            Text("ataque:")
                                .font(.footnote)
                        }
                        .progressViewStyle(.linear)
                        .frame(width: 200.0)
                        
                        //Defesa:
                        ProgressView(value: Float(pokemon.def), total: 100){
                        Text("Defesa:")
                            .font(.footnote)
                        }
                        .progressViewStyle(.linear)
                        .frame(width: 200.0)
                        .accentColor(.brown)
                        
                        /*XP:
                        ProgressView(value: Float(60), total: 100.0){
                            Text("Experiencia:")
                                .font(.footnote)
                        }
                        .progressViewStyle(.linear)
                        .frame(width: 100)
                        .accentColor(.yellow)
                         */
                    }//Group
                    
                }//VStack
                .padding(.bottom, 50.0)
                GifImage("\(pokemon.nome)".lowercased())
                    .frame(width: 310.0, height: 310.0)
            }//VStack
            
    }//body
}//pokeDetalhes


struct pokeDetalhesView_Previews: PreviewProvider {
    
    //var arrayPokemon: [Pokemon] = []
    static var previews: some View {
        /*
        let pokemon1:[Pokemon] = [Pokemon(nome: "Bulbasaur", img: "bulbasaur", atk:49, def:0.51, hp: 100)]
        */
        pokeDetalhes(pokemon: Pokemon(nome: "Bulbasaur", img: "bulbasaur", atk:49, def:0.51, hp:100, xp: 0, lvl: 0)/*, arrayPokemon: pokemon1*/)
    }
    
}//pokeDetalhesView_Previews
