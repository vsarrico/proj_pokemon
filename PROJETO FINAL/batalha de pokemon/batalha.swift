//
//  batalha.swift
//  pokedex
//
//  Created by Viorel on 27/12/2021.
//

import SwiftUI
import PhotosUI



struct batalha: View{
    
    //Funcoes:
    func fight (pokemon1:Pokemon, pokemon2:Pokemon) -> String{
        var winner: String = ""
        
        if pokemon1.hp != 0 && pokemon2.hp != 0
        {
            pokemon1.hp = pokemon1.hp - (pokemon2.atk > pokemon1.hp ? pokemon1.hp : pokemon2.atk) //Ou é uma ou outra.
            pokemon2.hp = pokemon2.hp - (pokemon1.atk > pokemon2.hp ? pokemon2.hp : pokemon1.atk) //Ou é uma ou outra.
            
            print("A vida do pokemon 1: \(pokemon1.hp).\nA vida do pokemon 2: \(pokemon2.hp)")
            if (pokemon1.hp > 0){
                return String("O oponente está a \(pokemon1.hp)% de vida")
            }
            
        }
        
        if pokemon1.hp == 0 || pokemon2.hp == 0
        {
            if pokemon1.hp > pokemon2.hp
            {
                winner = "\(pokemon1.nome) venceu."
                pokemon1.xp = 100
                pokemon1.lvl += 1
            }
            else if pokemon1.hp < pokemon2.hp            {
                winner = "Parabens \(pokemon2.nome)! venceu"
                pokemon2.xp = 100
                pokemon2.lvl += 1
            }
            else
            {
                winner = "Empate!"
            }
            
        }

        print("Vida do primeiro pokemon: ", pokemon1.hp)
        print("Vida do segundo pokemon: ", pokemon2.hp)
        print("O vencedor: ", winner)
        return winner
    }//fight

    func regeneracao(pokemon1:Pokemon, pokemon2:Pokemon) -> String{
        let hp: Int = 100
        
        pokemon1.hp = hp
        pokemon2.hp = hp
        print(pokemon1.hp)
        print(pokemon2.hp)
        return "As vidas foram regeneradas"
    }//regeneracao
    func cura(pokemon1:Pokemon, pokemon2:Pokemon) -> String{
        let potion: Int = 30
        
        pokemon2.hp = pokemon2.hp + potion
        print(pokemon2.hp)
        
        return "Curou: + \(potion) de vida"
    }//cura
    func veneno(pokemon1:Pokemon, pokemon2:Pokemon) -> String{
        let potion: Int = 15
        
        pokemon1.hp = pokemon1.hp - potion
        print(pokemon1.hp)
        return "O oponente perdeu -\(potion) de vida"
    }//veneno
    func addPokemon() -> String {
        Pokedex.append(Pokemon(nome: nome, img: "bulbasaur", atk: atk, def:0, hp:100, xp:0, lvl:0))
        
        print("ADICIONADO: \(nome), \(atk), \(hp)")
        
        print("Ataque: \(atk), do tipo: ", type(of: atk))
        //Imprimir todo o array:
        for i in 0 ..< Pokedex.count  {
            print(print(Pokedex[i].nome, Pokedex[i].atk, Pokedex[i].def, Pokedex[i].hp, Pokedex[i].xp, Pokedex[i].lvl))
        }
        
        return "Pokemon adiconado à sua lista"
    }//addPokemon
    func DeletePokemon() -> String{
        
        Pokedex.remove(at: 5)
        nome = ""
        
        //Imprimir todo o array:
        for i in 0 ..< Pokedex.count  {
            print(print(Pokedex[i].nome, Pokedex[i].atk, Pokedex[i].def, Pokedex[i].hp, Pokedex[i].xp, Pokedex[i].lvl))
        }
        
        return "O seu Pokemon foi removido"
    }//DeletePokemon
    func loadImage() {
        guard let inputImage = inputImage else { return }
        image = Image(uiImage: inputImage)
    }//LoadImage
    
    func StrToInt(myString:String) -> NSNumber?{
        let myString:String = ""
        let myInt:NSNumber
        
        myInt = NSNumber(value: (myString as NSString).integerValue)
        return myInt
    }
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    
    //Variaveis:
    @State var pokemon:Pokemon
    @State var Pokedex:[Pokemon] = [
        Pokemon(nome: "bulbasaur", img: "bulbasaur", atk:49, def:0.51, hp: 100, xp: 0, lvl: 0),
        Pokemon(nome: "charmander", img: "charmander", atk:52, def:0.57, hp: 100, xp: 0, lvl: 0),
        Pokemon(nome: "squirtle", img: "squirtle", atk:48, def:0.35, hp: 100, xp: 0, lvl: 0),
        Pokemon(nome: "pikachu", img: "pikachu", atk:55, def:0.40, hp:100, xp: 0, lvl: 0),
        Pokemon(nome: "raichu", img: "raichu", atk:80, def:0.50, hp:100, xp: 0, lvl: 0)
    ]
    @State var txt:String = ""
    @State private var Select1 = 0
    @State private var Select2 = 0
    @State var potion1 = true
    @State var potion2 = true
    @State var potion3 = true
    @State private var showingSheet = false
    @State private var showingSheet2 = false
    //NOVO POKEMON
    @State var nome:String = ""
    @State var atk:Int = 0
    @State var hp:Int = 100
    //Adicionar Imagem
    @State private var showingImagePicker = false
    @State private var inputImage: UIImage?
    @State var image:Image?
    @State var escolhido:String = ""
    @State var cor:Color
    @State var cor2:Color
    //Apagar pokemon
    @State var confirmar = false
    @State private var showDetails = false
    
    
    var body: some View{
        
        VStack{
            
            //Selecionar pokemon 1:
            if self.Select1 == 0{
                VStack{
                    Text("Selecione o oponente:").fontWeight(.bold)
                    Picker(selection: $Select1, label: Text("SELECIONAR 1")) {
                        Text("Nunhum selecionado").tag(0)
                        Text("Bulbusaur").tag(1)
                        Text("Charmander").tag(2)
                        Text("Squirtle").tag(3)
                        Text("Pikachu").tag(4)
                        Text("Raichu").tag(5)
                    }
                    .foregroundColor(.black)
                    .padding(.vertical, 10)
                    .frame(width: 200.0, height: 60)
                    .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 2)
                }
                .padding(10.0)
                .foregroundColor(.black)
                
            }
            if self.Select1 != 0{
                HStack{
                    //Pokemon1:
                    VStack{
                        
                        HStack{
                            Text("\(self.Pokedex[Select1-1].nome)".uppercased())
                                .padding(.bottom, 5.0)
                            Text("lvl: \(self.Pokedex[Select1-1].lvl)")
                                .padding(.leading, 40.0)
                        }
                        
                        
                        ProgressView(value: Float(self.Pokedex[Select1-1].hp), total: 100.0)
                        {
                            Text("Vida:")
                                .font(.footnote)
                        }
                        .frame(width: 180.0)
                        .accentColor(.red)
                        
                        ProgressView(value: Float(self.Pokedex[Select1-1].xp), total: 100.0)
                        {
                            Text("XP")
                                .font(.footnote)
                                .padding(.leading, 160.0)
                        }
                        .frame(width: 180.0)
                        .accentColor(Color.yellow)
                        
                    }
                    .frame(width: 220.0, height: 130.0)
                    .border(Color.black, width: 3)
                    .cornerRadius(10.0)
                    
                    
                    Image("\(self.Pokedex[Select1-1].nome)").resizable().frame(width: 100, height: 100)
                }
            }
            
            Text("\n \(txt) \n")
                .fontWeight(.bold)
                .padding(.bottom,5)
            
            //Selecionar pokemon2:
            if self.Select2 == 0{
                VStack{
                    Text("Selecine o seu Pokemon:").fontWeight(.bold)
                    Picker(selection: $Select2, label: Text("SELECIONAR 2")) {
                        Text("Nunhum selecionado").tag(0)
                        Text("Bulbusaur").tag(1)
                        Text("Charmander").tag(2)
                        Text("Squirtle").tag(3)
                        Text("Pikachu").tag(4)
                        Text("Raichu").tag(5)
                        Text(nome).tag(6)
                    }
                    .foregroundColor(.black)
                    .padding(.vertical, 10)
                    .frame(width: 200.0, height: 60)
                    .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 2)
                    
                    //Novo pokemon:
                    if self.Select2 == 0{
                        //Criar Pokemon:
                        if confirmar == false{
                            Button("Criar meu pokemon"){
                                showDetails.toggle()
                            }
                            .foregroundColor(.black)
                            .padding(.vertical, 5)
                            .frame(width: 200.0, height: 40)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 2)
                            if showDetails{
                                HStack{
                                    Text("Nome: ")
                                    TextField("Digite o nome", text: $nome)
                                        .frame(width: 140, height: 30)
                                        .disableAutocorrection(true)
                                        .multilineTextAlignment(/*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        .foregroundColor(.black)
                                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 1)
                                    
                                }
                                .frame(width: 200, height: 60)
                                
                                HStack{
                                    Text("Ataque: ")
                                    NumberTextField(value: $atk)
                                        .textFieldStyle(RoundedBorderTextFieldStyle())
                                        .disableAutocorrection(true)
                                        .multilineTextAlignment(/*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        .foregroundColor(.black)
                                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 1)
                                }
                                .frame(width: 200, height: 30)
                                
                                Text("Imagem: ")
                                    .padding(.bottom, 5.0)
                                    .padding(.trailing, 20.0)
                                HStack{
                                    
                                    Image("avatar1")
                                        .resizable()
                                        .frame(width: 100.0, height: 100.0)
                                        .onTapGesture{
                                            escolhido = "avatar1"
                                            cor = Color.black
                                        }
                                        .foregroundColor(cor)
                                        .border(cor, width: 1)
                                    
                                    Image("avatar2")
                                        .resizable()
                                        .frame(width: 100.0, height: 100.0)
                                        .onTapGesture{
                                            escolhido = "avatar2"
                                            cor2 = Color.black
                                        }
                                        .foregroundColor(cor2)
                                        .border(cor2, width: 1)
                                    
                                    Image(systemName: "plus.square")
                                        .resizable()
                                        .frame(width: 60.0, height: 60.0)
                                        .onTapGesture{
                                            showingImagePicker = true
                                        }
                                    
                                }
                                .padding()
                                
                                Button("ADICIONAR"){
                                    self.txt = addPokemon()
                                    showDetails = false
                                    confirmar = true
                                }
                                .frame(width: 200.0, height: 40)
                                .foregroundColor(.black)
                                .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 2)
                            }
                        }
                        else{
                            Button("Apagar meu pokemon"){
                                self.txt = DeletePokemon()
                                confirmar = false
                            }
                            .foregroundColor(.black)
                            .frame(width: 200.0)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 2)
                        }
                        
                        
                    }
                }
                .padding(10.0)
                .foregroundColor(.black)
                
                
            }
            if self.Select2 != 0{
                HStack{
                    
                    if escolhido == ""{
                    Image("\(self.Pokedex[Select2-1].nome)")
                        .resizable().frame(width:100, height:100)
                    }else{
                        Image(escolhido)
                        .resizable().frame(width:100, height:100)
                    }
                    
                    VStack{
                        HStack{
                            
                            Text("\(self.Pokedex[Select2-1].nome)".uppercased())
                                .padding(.bottom, 5.0)
                            Text("lvl: \(self.Pokedex[Select2-1].lvl)")
                                .padding(.leading, 40.0)
                        }
                        
                        ProgressView(value: Float(self.Pokedex[Select2-1].hp), total: 100.0)
                        {
                            Text("Vida:")
                                .font(.footnote)
                        }.frame(width: 180.0)
                            .accentColor(Color.red)
                        
                        ProgressView(value: Float(self.Pokedex[Select2-1].xp), total: 100.0)
                        {
                            Text("XP")
                                .font(.footnote)
                                .padding(.leading, 160.0)
                        }.frame(width: 180.0)
                            .accentColor(Color.yellow)
                        
                    }
                    .frame(width: 220.0, height: 130.0)
                    .border(Color.black, width: 3)
                    .cornerRadius(10.0)
                    
                }.padding(.bottom, 20.0)
            }
            
            //Batalha:
            if self.Select1 != 0 && self.Select2 != 0{
                VStack{
                    
                    VStack{
                        Text("Quem vence esta batalha?")
                            .fontWeight(.bold)
                        Text("Quem ataca tambem é atacado")
                    }
                    HStack{
                        //Movimentos
                        VStack(alignment:.center){
                            
                            Button("Atacar")
                            {
                                self.txt = fight(
                                    pokemon1: self.Pokedex[Select1-1],
                                    pokemon2: self.Pokedex[Select2-1])
                                
                                //lvlup(pokemon2: self.Pokedex[Select2-1])
                            }
                            .foregroundColor(.black)
                            .padding(.vertical, 10)
                            .frame(width: 100.0)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 2)
                            
                            Button("Desistir") {
                                self.Select2 = 0
                                self.txt = "Fugiu! Escolha outro pokemon"
                            }//Mochila
                            
                             
                        }
                        
                        //Pocoes
                        VStack{
                            HStack{
                                Text("Usar poção:")
                                    .padding(.vertical, 10)
                                    .frame(width: 100.0)
                                
                                Button(){
                                    showingSheet.toggle()
                                }label:{
                                    Image(systemName: "info")
                                }.sheet(isPresented: $showingSheet) {
                                    SheetView()
                                }
                                .foregroundColor(.blue)
                            }
                            
                            
                            HStack{
                                
                                Button(){
                                    self.txt = regeneracao(
                                        pokemon1: self.Pokedex[Select1-1],
                                        pokemon2: self.Pokedex[Select2-1])
                                }label:{
                                    Image(systemName: "cross.fill").padding(10)
                                }
                            .foregroundColor(.green)
                            .padding(10)
                            .frame(width: 20, height: 20)
                            .border(Color.green, width: 2)
                                
                                if (potion2){
                                    Button(){
                                        potion2 = false
                                        self.txt = veneno(
                                            pokemon1: self.Pokedex[Select1-1],
                                            pokemon2: self.Pokedex[Select2-1])
                                    }
                                label:{
                                    Image(systemName: "gobackward.15").padding(10)
                                }
                                .foregroundColor(.red)
                                .padding(10)
                                .frame(width: 20, height: 20)
                                .border(Color.red, width: 2)
                                }
                                if (potion3){
                                    Button(){
                                        potion3 = false
                                        self.txt = cura(
                                            pokemon1: self.Pokedex[Select1-1],
                                            pokemon2: self.Pokedex[Select2-1])
                                    }
                                label:{
                                    Image(systemName: "goforward.30").padding(10)
                                }
                                .foregroundColor(.green)
                                .padding(10)
                                .frame(width: 20, height: 20)
                                .border(Color.green, width: 2)
                                }
                                
                            }
                            
                        }
                        .foregroundColor(.black)
                        .padding(.vertical, 10)
                        .frame(width: 140.0, height: 90)
                        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 2)
                    }
                }//VStack
                .frame(width: 300.0, height: 220.0)
                .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: /*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
            }
           
        }//VStack Principal
        .background(Image("pokeballs"))
        .onChange(of: inputImage) { _ in loadImage() }
        .sheet(isPresented: $showingImagePicker) {
            ImagePicker(image: $inputImage)
        }
    }//body
}//batalha


struct SheetView: View{
    @Environment(\.dismiss) var dismiss
    
    var body: some View{
        
        VStack(alignment: .center){
            Text("Lista de poções existentes:")
                .offset(x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: /*@START_MENU_TOKEN@*/-100.0/*@END_MENU_TOKEN@*/)
            
            VStack{
                HStack(alignment: .bottom){
                    Image(systemName: "cross.fill")
                        .foregroundColor(.green)
                        .padding(10)
                        .frame(width: 20, height: 20)
                        .border(Color.green, width: 2)
                    Text("Cura instantaneamente a vida")
                }
                .padding(.trailing, 30.0)
                .frame(width: 300.0, height: 40.0)
                .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 1)
                
                HStack{
                    Image(systemName: "gobackward.15")
                        .foregroundColor(.red)
                        .padding(10)
                        .frame(width: 20, height: 20)
                        .border(Color.red, width: 2)
                    Text("Envenena o oponente")
                }
                .padding(.trailing, 90.0)
                .frame(width: 300.0, height: 40.0)
                .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 1)
                
                HStack{
                    Image(systemName: "goforward.30")
                        .foregroundColor(.green)
                        .padding(10)
                        .frame(width: 20, height: 20)
                        .border(Color.green, width: 2)
                    Text("Recupera 30 de vida")
                }
                .padding(.trailing, 100.0)
                .frame(width: 300.0, height: 40.0)
                .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 1)
                
            }
            
        }//VStack
        
        Button("Voltar") {
            dismiss()
        }
        .foregroundColor(.black)
        .padding(.vertical, 10).frame(width: 120.0, height: 70.0)
        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 2)
        .offset(x: 100, y: 100)
        
    }//body
    
}//SheetView Potions

struct ImagePicker: UIViewControllerRepresentable {
    @Binding var image: UIImage?
    
    func makeUIViewController(context: Context) -> PHPickerViewController {
        var config = PHPickerConfiguration()
        config.filter = .images
        let picker = PHPickerViewController(configuration: config)
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, PHPickerViewControllerDelegate {
        let parent: ImagePicker
        
        init(_ parent: ImagePicker) {
            self.parent = parent
        }
        
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            picker.dismiss(animated: true)
            
            guard let provider = results.first?.itemProvider else { return }
            
            if provider.canLoadObject(ofClass: UIImage.self) {
                provider.loadObject(ofClass: UIImage.self) { image, _ in
                    self.parent.image = image as? UIImage
                }
            }
        }
    }
}//ImagePicker

struct NumberTextField<V>: UIViewRepresentable where V: Numeric & LosslessStringConvertible {
    
    typealias UIViewType = UITextField
    
    // MARK: - Properties
    @Binding var value: V
    
    // MARK: - Functions
    func updateUIView(_ editField: UITextField, context: UIViewRepresentableContext<NumberTextField>) {
        editField.text = String(value)
    }
    
    func makeUIView(context: UIViewRepresentableContext<NumberTextField>) -> UITextField {
        let editField = UITextField()
        editField.delegate = context.coordinator
        editField.keyboardType = .numberPad
        return editField
    }
    
    func makeCoordinator() -> NumberTextField.Coordinator {
        Coordinator(value: $value)
    }
    
    class Coordinator: NSObject, UITextFieldDelegate {
        // MARK: - Properties
        var value: Binding<V>
        // MARK: - Life Cycle
        init(value: Binding<V>) {
            self.value = value
        }
        // MARK: - Functions
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let text = textField.text as NSString?
            let newValue = text?.replacingCharacters(in: range, with: string)
            if let number = V(newValue ?? "0") {
                self.value.wrappedValue = number
                return true
            } else {
                if nil == newValue || newValue!.isEmpty {
                    self.value.wrappedValue = 0
                }
                return false
            }
        }
        func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
            if reason == .committed {
                textField.resignFirstResponder()
            }
        }
    }
}//NumberTextField




struct batalha_Previews: PreviewProvider {
    static var previews: some View {
        batalha(pokemon: Pokemon(nome: "Bulbasaur", img: "bulbasaur", atk:49, def:0.51, hp: 100, xp: 0, lvl: 0), cor: Color.black, cor2: Color.black)
    }
}//batalha_Previews
