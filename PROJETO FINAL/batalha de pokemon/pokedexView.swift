//
//  pokedexView.swift
//  pokedex
//
//  Created by Viorel on 27/12/2021.
//

import SwiftUI

struct pokedexView: View {
    
    //Funcoes:
    func backgroundcolor (forType type:String) -> Color{
        switch type {
        case "ERVA": return Color.green
        case "FOGO": return Color.red
        case "AGUA": return Color.blue
        case "ELETRICO": return Color.yellow
        default: return Color.pink
        }
    }
    
    //Variaveis:
    @State var Pokedex:[tipoPokemon] = [
        
        tipoPokemon(tipo: "Erva", pokemons:[
            Pokemon(nome: "Bulbasaur", img: "bulbasaur", atk:49, def:49, hp:100, xp: 0, lvl: 0),
            Pokemon(nome: "Ivysaur", img: "ivysaur", atk:62, def:63, hp:100, xp: 0, lvl: 0),
            Pokemon(nome: "Venusaur", img: "venusaur", atk:82, def:83, hp:100, xp: 0, lvl: 0),
        ]),
        
        tipoPokemon(tipo: "Fogo", pokemons:[
            Pokemon(nome: "Charmander", img: "charmander", atk:39, def:60, hp:100, xp: 0, lvl: 0),
            Pokemon(nome: "Charmeleon", img: "charmeleon", atk:58, def:80, hp:100, xp: 0, lvl: 0),
            Pokemon(nome: "Charizard", img: "charizard", atk:79, def:109, hp:100, xp: 0, lvl: 0)
        ]),
        
        tipoPokemon(tipo: "Agua", pokemons:[
            Pokemon(nome: "Squirtle", img: "squirtle", atk:48, def:65, hp:100, xp: 0, lvl: 0),
            Pokemon(nome: "Wartortle", img: "wartortle", atk:63, def:80, hp:100, xp: 0, lvl: 0),
            Pokemon(nome: "Blastoise", img: "blastoise", atk:83, def:100, hp:100, xp: 0, lvl: 0)
        ]),
        
        tipoPokemon(tipo: "Eletrico", pokemons:[
            Pokemon(nome: "Pikachu", img: "pikachu", atk:55, def:40, hp:100, xp: 0, lvl: 0),
            Pokemon(nome: "Raichu", img: "raichu", atk:80, def:50, hp:100, xp: 0, lvl: 0)
        ]),
    ]
    /*
    var searchResults: [tipoPokemon] {
        if searchText.isEmpty {
            return Pokedex
        } else {
            /*
             var resultTipo: [tipoPokemon]
             
             for tipo in Pokedex {
             
             for pokemon in tipo.pokemons{
             if pokemon.nome == searchText{
             resultTipo == pokemon
             }
             }
             }
             */
            return Pokedex.filter { $0.tipo == searchText }
            //return resultTipo
        }
    }
     */
    
    @State private var searchText = ""
    //var arrayPokemon:[Pokemon] = []
    
    var body: some View {

        
        NavigationView{
                    List{
                        ForEach(Pokedex){ tipo in
                            Section {
                                ForEach(tipo.pokemons){ pokemon in
                                    NavigationLink {
                                        pokeDetalhes(pokemon: pokemon)
                                    } label: {
                                        ListLine(pokemon: pokemon)
                                    }
                                }
                                .background((backgroundcolor(forType: tipo.tipo.uppercased())))
                                .foregroundColor(Color.white)
                                
                            } header: {
                                Text(tipo.tipo)
                            }//Selection
                        }//ForEach
                        .navigationTitle("Pokedex")
                    }//List
            
            
                }//NavigationView
    }//View body
}//View


struct pokedexView_Previews: PreviewProvider {
    static var previews: some View {
        pokedexView()
    }
}
